package com.Puzzlegame;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;

public class ButtonObject extends JButton
{
	Image image_closed;
	Image image_open;
	Image currentimagetobeshown;
	ButtonObject(Image im ) throws Exception
	{
		super();
		image_closed=ImageIO.read(new File("/home/ishanmishra/Desktop/Pics/pic_main.jpg"));
		this.image_open=im;
		this.currentimagetobeshown=image_closed;
	}
	void displayImage()
	{
		currentimagetobeshown=image_open;
		repaint();
	}
	void hideImage()
	{
		currentimagetobeshown=image_closed;
		repaint();
	}
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		g.drawImage(currentimagetobeshown, 0, 0, 100, 150, this);
	}
	public Dimension getPreferredSize()
	{
		return new Dimension(100, 150);
	}
}
