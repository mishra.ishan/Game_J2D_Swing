package com.Puzzlegame;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.border.EmptyBorder;

public class Puzzle extends JFrame
{
	int count=0;
	JPanel centerpanel;
	int pics_remaining;
	private String[] image_location;
	private boolean button_clicked=false;
	Image lastclickedimage;
	ButtonObject previousclickedbutton;
	boolean getButtonClickedStatus()
	{
		return button_clicked;
	}
	void toggleButtonClickedStatus()
	{
		if(button_clicked==true)
		{
			button_clicked=false;
		}
		else
			button_clicked=true;
	}
	Puzzle(String[] loc) throws Exception
	{
		image_location=loc;
		pics_remaining=image_location.length;
		init();
	}
	public void init() throws Exception
	{
		JPanel topmenu=new JPanel();
		add(topmenu, BorderLayout.NORTH);
		JMenuBar menubar=new JMenuBar();
		setJMenuBar(menubar);
		JMenu menu=new JMenu("Menu");
		JMenuItem newgame=new JMenuItem("New Game");
		MyActionListenerForANewGame newgameal=new MyActionListenerForANewGame(this);
		newgame.addActionListener(newgameal);
		JMenuItem exitgame=new JMenuItem("Exit");
		exitgame.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				System.exit(0);
			}
		});
		menubar.add(menu);
		menu.add(newgame);
		menu.add(exitgame);
		centerpanel=new JPanel();
		centerpanel.setLayout(new GridLayout(3, 3, 100, 100));
		centerpanel.setBorder(new EmptyBorder(50,50,50,50));
		add(centerpanel, BorderLayout.CENTER);
		centerpanel.setSize(new Dimension(500, 500));
		ImageIcon folder=new ImageIcon("pic_main.jpg");
		 Random rnd = new Random();
		 for (int i = image_location.length - 1; i > 0; i--)
		 {
		     int index = rnd.nextInt(i + 1);
		     String a = image_location[index];
		     image_location[index] = image_location[i];
		     image_location[i] = a;
		}
		final ArrayList<ButtonObject> button = new ArrayList();
		ImageIcon temp=null;
		//Image temp=null;
		for(int i=0; i<image_location.length; i++)
		{			
			temp=new ImageIcon(image_location[i]);
			//temp=ImageIO.read(new File(image_location[i]));
			button.add(new ButtonObject(temp.getImage()));
			//button.add(new ButtonObject(temp));
			centerpanel.add(button.get(i));
		}
		for(int i=0; i<12; i++)
		{
			MyActionListener al=new MyActionListener(this, button.get(i));
			button.get(i).addActionListener(al);
		}
		
		pack();
	}
	
	class MyActionListener implements ActionListener
	{
		Puzzle puz;
		ButtonObject but;
		MyActionListener(Puzzle p, ButtonObject b)
		{
			puz=p;
			but=b;
		}
		@Override
		public void actionPerformed(ActionEvent e)
		{			
			but.displayImage();
			count++;
			if(puz.getButtonClickedStatus()==false)
			{
				puz.toggleButtonClickedStatus();
				puz.lastclickedimage=but.image_open;
				puz.previousclickedbutton=but;
			}
			else if(puz.getButtonClickedStatus()==true&&puz.lastclickedimage.equals(but.image_open)&&(!puz.previousclickedbutton.equals(but)))
			{
				try
				{
					System.out.println("Exec");
					but.setVisible(false);
					puz.toggleButtonClickedStatus();
					puz.previousclickedbutton.setVisible(false);
					puz.pics_remaining=puz.pics_remaining-2;
					if(pics_remaining==0)
					{
						JOptionPane.showMessageDialog(puz.centerpanel, (Object)count, "No of moves taken to finish", JOptionPane.INFORMATION_MESSAGE);
					}
				}
				catch(Exception ex)
				{
					ex.printStackTrace();
				}
			}
			else if(puz.getButtonClickedStatus()==true&&puz.lastclickedimage!=but.image_open)
			{
				try
				{
					puz.previousclickedbutton.hideImage();
					but.hideImage();
					puz.toggleButtonClickedStatus();
				}
				catch(Exception ex)
				{
					ex.printStackTrace();
				}
			}
		}
	}
	
	/*class MyTimerListener implements ActionListener
	{
		Puzzle puz;
		ButtonObject but;
		MyTimerListener(Puzzle p, ButtonObject b)
		{
			puz=p;
			but=b;
		}
		@Override
		public void actionPerformed(ActionEvent e) 
		{
			timer.stop();
			
		}
	}*/
	
	class MyActionListenerForANewGame implements ActionListener
	{
		Puzzle puz;
		public MyActionListenerForANewGame(Puzzle p) 
		{
			puz=p;
		}
		@Override
		public void actionPerformed(ActionEvent e) 
		{
			if(puz.getButtonClickedStatus()==true)
			{
				toggleButtonClickedStatus();
			}
			SwingUtilities.invokeLater(new Runnable() {
				
				@Override
				public void run() 
				{
					try
					{
						Puzzle puzzle=new Puzzle(image_location);
						puzzle.setVisible(true);
					}
					catch(Exception ex)
					{
						ex.printStackTrace();
					}
				}
			});
		}
	}
	
	public static void main(String args[]) throws Exception
	{
		final String[] im_loc={"rsz_pic1.jpg", "rsz_1pic2.jpg","rsz_pic3.jpg","rsz_pic4.jpg","rsz_pic5.jpg",
				"rsz_pic6.jpg", "rsz_pic1.jpg", "rsz_1pic2.jpg","rsz_pic3.jpg","rsz_pic4.jpg","rsz_pic5.jpg",
				"rsz_pic6.jpg"};
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				try
				{
				Puzzle puzzle=new Puzzle(im_loc);
				puzzle.setVisible(true);
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}
			}
		});
	}
}